add_library(opensimplexnoise STATIC
	open_simplex_noise.cpp
	open_simplex_noise2f.cpp
	open_simplex_noise2s.cpp
	simplex_util.cpp
)
