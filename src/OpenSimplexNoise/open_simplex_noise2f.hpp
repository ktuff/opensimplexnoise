/**
 * K.jpg's OpenSimplex 2, smooth variant ("SuperSimplex")
 *
 * - 2D is standard simplex, modified to support larger kernels.
 *   Implemented using a lookup table.
 * - 3D is "Re-oriented 8-point BCC noise" which constructs a
 *   congruent BCC lattice in a much different way than usual.
 * - 4D uses a naïve pregenerated lookup table, and averages out
 *   to the expected performance.
 *
 * Multiple versions of each function are provided. See the
 * documentation above each, for more info.
 *
 * modified on 2020-10-18 by Toffel
 */

#pragma once

#include <stdint.h>
#include <array>
#include <vector>

#include "simplex_util.hpp"

struct lattice_point4df {
    int xsv, ysv, zsv, wsv;
    double dx, dy, dz, dw;
    double xsi, ysi, zsi, wsi;
    double ssiDelta;

    lattice_point4df();
    lattice_point4df(int xsv, int ysv, int zsv, int wsv);
};


class open_simplex_noise2f {

    private:
        static const size_t PSIZE = 2048;
        static const size_t PMASK = 2047;

        static const lattice_point2d LOOKUP_2D[4];
        static const std::array<lattice_point3d, 64>* LOOKUP_3D;
        static const std::array<lattice_point4df, 16>* LOOKUP_4D;

        static const std::array<grad2, PSIZE>* GRADIENTS_2D;
        static const std::array<grad3, PSIZE>* GRADIENTS_3D;
        static const std::array<grad4, PSIZE>* GRADIENTS_4D;

        int16_t m_perm[PSIZE];
        grad2 m_perm_grad2[PSIZE];
        grad3 m_perm_grad3[PSIZE];
        grad4 m_perm_grad4[PSIZE];


    public:
        open_simplex_noise2f(uint64_t seed);
        ~open_simplex_noise2f();


    public:
        void set_seed(uint64_t seed);

        double noise_2d_classic(double x, double y) const;
        double noise_2d_XbeforeY(double x, double y) const;

        double noise_3d_classic(double x, double y, double z) const;
        double noise_3d_XYbeforeZ(double x, double y, double z) const;
        double noise_3d_XZbeforeY(double x, double y, double z) const;

        double noise_4d_classic(double x, double y, double z, double w) const;
        double noise_4d_XYbeforeZW(double x, double y, double z, double w) const;
        double noise_4d_XZbeforeYW(double x, double y, double z, double w) const;
        double noise_4d_XYZbeforeW(double x, double y, double z, double w) const;

    private:
        double noise_2d_base(double x, double y) const;
        double noise_3d_base(double x, double y, double z) const;
        double noise_4d_base(double x, double y, double z, double w) const;

	private:
		void init_static_data();
};
