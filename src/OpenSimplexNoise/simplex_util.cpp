/**
 * K.jpg's OpenSimplex 2, smooth variant ("SuperSimplex")
 *
 * - 2D is standard simplex, modified to support larger kernels.
 *   Implemented using a lookup table.
 * - 3D is "Re-oriented 8-point BCC noise" which constructs a
 *   congruent BCC lattice in a much different way than usual.
 * - 4D uses a naïve pregenerated lookup table, and averages out
 *   to the expected performance.
 *
 * Multiple versions of each function are provided. See the
 * documentation above each, for more info.
 *
 * modified on 2020-10-18 by KTuff
 */

#include "simplex_util.hpp"

lattice_point2d::lattice_point2d()
{
}

lattice_point2d::lattice_point2d(int xsv, int ysv)
{
    this->xsv = xsv;
    this->ysv = ysv;
    double ssv = (xsv + ysv) * -0.211324865405187;
    this->dx = -xsv - ssv;
    this->dy = -ysv - ssv;
}

lattice_point3d::lattice_point3d()
{
}

lattice_point3d::lattice_point3d(int xrv, int yrv, int zrv, int lattice, const lattice_point3d* fail, const lattice_point3d* succ)
{
    this->next_on_failure = fail;
    this->next_on_success = succ;
    this->dxr = -xrv + lattice * 0.5;
    this->dyr = -yrv + lattice * 0.5;
    this->dzr = -zrv + lattice * 0.5;
    this->xrv = xrv + lattice * 1024;
    this->yrv = yrv + lattice * 1024;
    this->zrv = zrv + lattice * 1024;
}


/*
 * Gradients
 */

grad2::grad2()
: dx(0.0)
, dy(0.0)
{
}

grad2::grad2(double dx, double dy)
{
    this->dx = dx;
    this->dy = dy;
}

grad3::grad3()
: dx(0.0)
, dy(0.0)
, dz(0.0)
{
}

grad3::grad3(double dx, double dy, double dz)
{
    this->dx = dx;
    this->dy = dy;
    this->dz = dz;
}

grad4::grad4()
: dx(0.0)
, dy(0.0)
, dz(0.0)
, dw(0.0)
{
}

grad4::grad4(double dx, double dy, double dz,  double dw)
{
    this->dx = dx;
    this->dy = dy;
    this->dz = dz;
    this->dw = dw;
}

