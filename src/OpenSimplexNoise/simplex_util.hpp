/**
 * K.jpg's OpenSimplex 2, smooth variant ("SuperSimplex")
 *
 * - 2D is standard simplex, modified to support larger kernels.
 *   Implemented using a lookup table.
 * - 3D is "Re-oriented 8-point BCC noise" which constructs a
 *   congruent BCC lattice in a much different way than usual.
 * - 4D uses a naïve pregenerated lookup table, and averages out
 *   to the expected performance.
 *
 * Multiple versions of each function are provided. See the
 * documentation above each, for more info.
 *
 * modified on 2020-10-18 by KTuff
 */

#pragma once

struct lattice_point2d {
    int xsv, ysv;
    double dx, dy;

    lattice_point2d();
    lattice_point2d(int xsv, int ysv);
};

struct lattice_point3d {
    double dxr, dyr, dzr;
    int xrv, yrv, zrv;
    const lattice_point3d *next_on_failure, *next_on_success;

    lattice_point3d();
    lattice_point3d(int xrv, int yrv, int zrv, int lattice, const lattice_point3d* fail, const lattice_point3d* succ);
};

/*
 * Gradients
 */

struct grad2 {
    double dx, dy;

    grad2();
    grad2(double dx, double dy);
};

struct grad3 {
    double dx, dy, dz;

    grad3();
    grad3(double dx, double dy, double dz);
};

struct grad4 {
    double dx, dy, dz, dw;

    grad4();
    grad4(double dx, double dy, double dz,  double dw);
};

