/**
 * K.jpg's OpenSimplex 2, smooth variant ("SuperSimplex")
 *
 * - 2D is standard simplex, modified to support larger kernels.
 *   Implemented using a lookup table.
 * - 3D is "Re-oriented 8-point BCC noise" which constructs a
 *   congruent BCC lattice in a much different way than usual.
 * - 4D uses a naïve pregenerated lookup table, and averages out
 *   to the expected performance.
 *
 * Multiple versions of each function are provided. See the
 * documentation above each, for more info.
 *
 * modified on 2020-10-18 by KTuff
 */
#include <cmath>

#include "open_simplex_noise2f.hpp"

lattice_point4df::lattice_point4df()
{
}

lattice_point4df::lattice_point4df(int xsv, int ysv, int zsv, int wsv)
{
    this->xsv = xsv + 409;
    this->ysv = ysv + 409;
    this->zsv = zsv + 409;
    this->wsv = wsv + 409;
    double ssv = (xsv + ysv + zsv + wsv) * 0.309016994374947;
    this->dx = -xsv - ssv;
    this->dy = -ysv - ssv;
    this->dz = -zsv - ssv;
    this->dw = -wsv - ssv;
    this->xsi = 0.2 - xsv;
    this->ysi = 0.2 - ysv;
    this->zsi = 0.2 - zsv;
    this->wsi = 0.2 - wsv;
    this->ssiDelta = (0.8 - xsv - ysv - zsv - wsv) * 0.309016994374947;
}


/*
 * DEFINITIONS
 */

const lattice_point2d open_simplex_noise2f::LOOKUP_2D[4] = {
	lattice_point2d(1, 0),
	lattice_point2d(0, 0),
	lattice_point2d(1, 1),
	lattice_point2d(0, 1)
};

static const double N2 = 0.01001634121365712;
static const double N3 = 0.030485933181293584;
static const double N4 = 0.009202377986303158;

/*
 * UTILITY
 */

static int fast_floor(double x) {
    int xi = (int)x;
    return (x < xi) ? xi-1 : xi;  // branchless: return xi - (x < xi);
}


const std::array<lattice_point3d, 64>* open_simplex_noise2f::LOOKUP_3D = nullptr;
const std::array<lattice_point4df, 16>* open_simplex_noise2f::LOOKUP_4D = nullptr;
const std::array<grad2, open_simplex_noise2f::PSIZE>* open_simplex_noise2f::GRADIENTS_2D = nullptr;
const std::array<grad3, open_simplex_noise2f::PSIZE>* open_simplex_noise2f::GRADIENTS_3D = nullptr;
const std::array<grad4, open_simplex_noise2f::PSIZE>* open_simplex_noise2f::GRADIENTS_4D = nullptr;


open_simplex_noise2f::open_simplex_noise2f(uint64_t seed)
{
    open_simplex_noise2f::init_static_data();

    this->set_seed(seed);
}

open_simplex_noise2f::~open_simplex_noise2f()
{
}


void open_simplex_noise2f::set_seed(uint64_t seed)
{
    int16_t source[PSIZE];
    for(size_t i=0; i<PSIZE; i++) {
        source[i] = int16_t(i);
    }
    for(int i=PSIZE-1; i>=0; i--) {
        seed = seed * 6364136223846793005ULL + 1442695040888963407ULL;
        int r = (int)((seed + 31) % (i+1));
        if(r < 0) {
            r += (i+1);
        }
        this->m_perm[i] = source[r];
        this->m_perm_grad2[i] = (*GRADIENTS_2D)[m_perm[i]];
        this->m_perm_grad3[i] = (*GRADIENTS_3D)[m_perm[i]];
        this->m_perm_grad4[i] = (*GRADIENTS_4D)[m_perm[i]];
        source[r] = source[i];
    }
}

/*
 * Noise Evaluators
 */

/*
 * 2D SuperSimplex noise, standard lattice orientation.
 */
double
open_simplex_noise2f::noise_2d_classic(double x, double y) const
{
    // Get points for A2* lattice
    double s = 0.366025403784439 * (x + y);
    double xs = x + s, ys = y + s;

    return noise_2d_base(xs, ys);
}

/*
 * 2D SuperSimplex noise, with Y pointing down the main diagonal.
 * Might be better for a 2D sandbox style game, where Y is vertical.
 * Probably slightly less optimal for heightmaps or continent maps.
 */
double
open_simplex_noise2f::noise_2d_XbeforeY(double x, double y) const
{
    // Skew transform and rotation baked into one.
    double xx = x * 0.7071067811865476;
    double yy = y * 1.224744871380249;

    return noise_2d_base(yy + xx, yy - xx);
}

/*
 * 2D SuperSimplex noise base.
 * Lookup table implementation inspired by DigitalShadow.
 */
double
open_simplex_noise2f::noise_2d_base(double xs, double ys) const
{
    double value = 0;

    // Get base points and offsets
    int xsb = fast_floor(xs), ysb = fast_floor(ys);
    double xsi = xs - xsb, ysi = ys - ysb;

    // Index to point list
    int index = (int)((ysi - xsi) / 2 + 1);
    double ssi = (xsi + ysi) * -0.211324865405187;
    double xi = xsi + ssi, yi = ysi + ssi;

    // Point contributions
    for (int i = 0; i < 3; i++) {
        const lattice_point2d& c = LOOKUP_2D[index + i];

        double dx = xi + c.dx, dy = yi + c.dy;
        double attn = 0.5 - dx * dx - dy * dy;
        if (attn <= 0) {
            continue;
        }

        int pxm = (xsb + c.xsv) & PMASK, pym = (ysb + c.ysv) & PMASK;
        const grad2& grad = m_perm_grad2[m_perm[pxm] ^ pym];
        double extrapolation = grad.dx * dx + grad.dy * dy;

        attn *= attn;
        value += attn * attn * extrapolation;
    }
    return value;
}

/*
 * 3D Re-oriented 8-point BCC noise, classic orientation
 * Proper substitute for what 3D SuperSimplex would be,
 * in light of Forbidden Formulae.
 * Use noise3_XYBeforeZ or noise3_XZBeforeY instead, wherever appropriate.
 */
double
open_simplex_noise2f::noise_3d_classic(double x, double y, double z) const
{
    // Re-orient the cubic lattices via rotation, to produce the expected look on cardinal planar slices.
    // If texturing objects that don't tend to have cardinal plane faces, you could even remove this.
    // Orthonormal rotation. Not a skew transform.
    double r = (2.0 / 3.0) * (x + y + z);
    double xr = r - x, yr = r - y, zr = r - z;

    // Evaluate both lattices to form a BCC lattice.
    return noise_3d_base(xr, yr, zr);
}

/*
 * 3D Re-oriented 8-point BCC noise, with better visual isotropy in (X, Y).
 * Recommended for 3D terrain and time-varied animations.
 * The Z coordinate should always be the "different" coordinate in your use case.
 * If Y is vertical in world coordinates, call noise3_XYBeforeZ(x, z, Y) or use noise3_XZBeforeY.
 * If Z is vertical in world coordinates, call noise3_XYBeforeZ(x, y, Z).
 * For a time varied animation, call noise3_XYBeforeZ(x, y, T).
 */
double
open_simplex_noise2f::noise_3d_XYbeforeZ(double x, double y, double z) const
{
    // Re-orient the cubic lattices without skewing, to make X and Y triangular like 2D.
    // Orthonormal rotation. Not a skew transform.
    double xy = x + y;
    double s2 = xy * -0.211324865405187;
    double zz = z * 0.577350269189626;
    double xr = x + s2 - zz, yr = y + s2 - zz;
    double zr = xy * 0.577350269189626 + zz;

    // Evaluate both lattices to form a BCC lattice.
    return noise_3d_base(xr, yr, zr);
}

/*
 * 3D Re-oriented 8-point BCC noise, with better visual isotropy in (X, Z).
 * Recommended for 3D terrain and time-varied animations.
 * The Y coordinate should always be the "different" coordinate in your use case.
 * If Y is vertical in world coordinates, call noise3_XZBeforeY(x, Y, z).
 * If Z is vertical in world coordinates, call noise3_XZBeforeY(x, Z, y) or use noise3_XYBeforeZ.
 * For a time varied animation, call noise3_XZBeforeY(x, T, y) or use noise3_XYBeforeZ.
 */
double
open_simplex_noise2f::noise_3d_XZbeforeY(double x, double y, double z) const
{
    // Re-orient the cubic lattices without skewing, to make X and Z triangular like 2D.
    // Orthonormal rotation. Not a skew transform.
    double xz = x + z;
    double s2 = xz * -0.211324865405187;
    double yy = y * 0.577350269189626;
    double xr = x + s2 - yy;
    double zr = z + s2 - yy;
    double yr = xz * 0.577350269189626 + yy;

    // Evaluate both lattices to form a BCC lattice.
    return noise_3d_base(xr, yr, zr);
}

/*
 * Generate overlapping cubic lattices for 3D Re-oriented BCC noise.
 * Lookup table implementation inspired by DigitalShadow.
 * It was actually faster to narrow down the points in the loop itself,
 * than to build up the index with enough info to isolate 8 points.
 */
double
open_simplex_noise2f::noise_3d_base(double xr, double yr, double zr) const
{
    // Get base and offsets inside cube of first lattice.
    int xrb = fast_floor(xr), yrb = fast_floor(yr), zrb = fast_floor(zr);
    double xri = xr - xrb, yri = yr - yrb, zri = zr - zrb;

    // Identify which octant of the cube we're in. This determines which cell
    // in the other cubic lattice we're in, and also narrows down one point on each.
    int xht = (int)(xri + 0.5), yht = (int)(yri + 0.5), zht = (int)(zri + 0.5);
    int index = (xht << 0) | (yht << 1) | (zht << 2);

    // Point contributions
    double value = 0;
    lattice_point3d const * c = &(*LOOKUP_3D)[index];
    while (c != nullptr) {
        double dxr = xri + c->dxr, dyr = yri + c->dyr, dzr = zri + c->dzr;
        double attn = 0.5 - dxr * dxr - dyr * dyr - dzr * dzr;
        if (attn < 0) {
            c = c->next_on_failure;
        } else {
            int pxm = (xrb + c->xrv) & PMASK, pym = (yrb + c->yrv) & PMASK, pzm = (zrb + c->zrv) & PMASK;
            const grad3& grad = m_perm_grad3[m_perm[m_perm[pxm] ^ pym] ^ pzm];
            double extrapolation = grad.dx * dxr + grad.dy * dyr + grad.dz * dzr;

            attn *= attn;
            value += attn * attn * extrapolation;
            c = c->next_on_success;
        }
    }
    return value;
}

/*
 * 4D SuperSimplex noise, classic lattice orientation.
 */
double
open_simplex_noise2f::noise_4d_classic(double x, double y, double z, double w) const
{
    // Get points for A4 lattice
    double s = -0.138196601125011 * (x + y + z + w);
    double xs = x + s, ys = y + s, zs = z + s, ws = w + s;

    return noise_4d_base(xs, ys, zs, ws);
}

/*
 * 4D SuperSimplex noise, with XY and ZW forming orthogonal triangular-based planes.
 * Recommended for 3D terrain, where X and Y (or Z and W) are horizontal.
 * Recommended for noise(x, y, sin(time), cos(time)) trick.
 */
double
open_simplex_noise2f::noise_4d_XYbeforeZW(double x, double y, double z, double w) const
{
    double s2 = (x + y) * -0.178275657951399372 + (z + w) * 0.215623393288842828;
    double t2 = (z + w) * -0.403949762580207112 + (x + y) * -0.375199083010075342;
    double xs = x + s2, ys = y + s2, zs = z + t2, ws = w + t2;

    return noise_4d_base(xs, ys, zs, ws);
}

/*
 * 4D SuperSimplex noise, with XZ and YW forming orthogonal triangular-based planes.
 * Recommended for 3D terrain, where X and Z (or Y and W) are horizontal.
 */
double
open_simplex_noise2f::noise_4d_XZbeforeYW(double x, double y, double z, double w) const
{
    double s2 = (x + z) * -0.178275657951399372 + (y + w) * 0.215623393288842828;
    double t2 = (y + w) * -0.403949762580207112 + (x + z) * -0.375199083010075342;
    double xs = x + s2, ys = y + t2, zs = z + s2, ws = w + t2;

    return noise_4d_base(xs, ys, zs, ws);
}

/*
 * 4D SuperSimplex noise, with XYZ oriented like noise3_Classic,
 * and W for an extra degree of freedom.
 * Recommended for time-varied animations which texture a 3D object (W=time)
 */
double
open_simplex_noise2f::noise_4d_XYZbeforeW(double x, double y, double z, double w) const
{
    double xyz = x + y + z;
    double ww = w * 0.2236067977499788;
    double s2 = xyz * -0.16666666666666666 + ww;
    double xs = x + s2, ys = y + s2, zs = z + s2, ws = -0.5 * xyz + ww;

    return noise_4d_base(xs, ys, zs, ws);
}

/*
 * 4D SuperSimplex noise base.
 * Using ultra-simple 4x4x4x4 lookup partitioning.
 * This isn't as elegant or SIMD/GPU/etc. portable as other approaches,
 * but it does compete performance-wise with optimized OpenSimplex1.
 */
double
open_simplex_noise2f::noise_4d_base(double xs, double ys, double zs, double ws) const
{
    double value = 0;

    // Get base points and offsets
    int xsb = fast_floor(xs), ysb = fast_floor(ys), zsb = fast_floor(zs), wsb = fast_floor(ws);
    double xsi = xs - xsb, ysi = ys - ysb, zsi = zs - zsb, wsi = ws - wsb;

    // If we're in the lower half, flip so we can repeat the code for the upper half. We'll flip back later.
    double siSum = xsi + ysi + zsi + wsi;
    double ssi = siSum * 0.309016994374947; // Prep for vertex contributions.
    bool inLowerHalf = (siSum < 2);
    if (inLowerHalf) {
        xsi = 1 - xsi; ysi = 1 - ysi; zsi = 1 - zsi; wsi = 1 - wsi;
        siSum = 4 - siSum;
    }

    // Consider opposing vertex pairs of the octahedron formed by the central cross-section of the stretched tesseract
    double aabb = xsi + ysi - zsi - wsi, abab = xsi - ysi + zsi - wsi, abba = xsi - ysi - zsi + wsi;
    double aabbScore = std::abs(aabb), ababScore = std::abs(abab), abbaScore = std::abs(abba);

    // Find the closest point on the stretched tesseract as if it were the upper half
    int vertexIndex, via, vib;
    double asi, bsi;
    if (aabbScore > ababScore && aabbScore > abbaScore) {
        if (aabb > 0) {
            asi = zsi; bsi = wsi; vertexIndex = 0b0011; via = 0b0111; vib = 0b1011;
        } else {
            asi = xsi; bsi = ysi; vertexIndex = 0b1100; via = 0b1101; vib = 0b1110;
        }
    } else if (ababScore > abbaScore) {
        if (abab > 0) {
            asi = ysi; bsi = wsi; vertexIndex = 0b0101; via = 0b0111; vib = 0b1101;
        } else {
            asi = xsi; bsi = zsi; vertexIndex = 0b1010; via = 0b1011; vib = 0b1110;
        }
    } else {
        if (abba > 0) {
            asi = ysi; bsi = zsi; vertexIndex = 0b1001; via = 0b1011; vib = 0b1101;
        } else {
            asi = xsi; bsi = wsi; vertexIndex = 0b0110; via = 0b0111; vib = 0b1110;
        }
    }
    if (bsi > asi) {
        via = vib;
        double temp = bsi;
        bsi = asi;
        asi = temp;
    }
    if (siSum + asi > 3) {
        vertexIndex = via;
        if (siSum + bsi > 4) {
            vertexIndex = 0b1111;
        }
    }

    // Now flip back if we're actually in the lower half.
    if (inLowerHalf) {
        xsi = 1 - xsi; ysi = 1 - ysi; zsi = 1 - zsi; wsi = 1 - wsi;
        vertexIndex ^= 0b1111;
    }

    // Five points to add, total, from five copies of the A4 lattice.
    for (int i = 0; i < 4; i++) {
        // Update xsb/etc. and add the lattice point's contribution.
        const lattice_point4df& c = (*LOOKUP_4D)[vertexIndex];
        xsb += c.xsv; ysb += c.ysv; zsb += c.zsv; wsb += c.wsv;
        double xi = xsi + ssi, yi = ysi + ssi, zi = zsi + ssi, wi = wsi + ssi;
        double dx = xi + c.dx, dy = yi + c.dy, dz = zi + c.dz, dw = wi + c.dw;
        double attn = 0.5 - dx * dx - dy * dy - dz * dz - dw * dw;
        if (attn > 0) {
            int pxm = xsb & PMASK, pym = ysb & PMASK, pzm = zsb & PMASK, pwm = wsb & PMASK;
            const grad4& grad = m_perm_grad4[m_perm[m_perm[m_perm[pxm] ^ pym] ^ pzm] ^ pwm];
            double ramped = grad.dx * dx + grad.dy * dy + grad.dz * dz + grad.dw * dw;

            attn *= attn;
            value += attn * attn * ramped;
        }

        // Update the relative skewed coordinates to reference the vertex we just added.
        // Rather, reference its counterpart on the lattice copy that is shifted down by
        // the vector <-0.2, -0.2, -0.2, -0.2>
        xsi += c.xsi; ysi += c.ysi; zsi += c.zsi; wsi += c.wsi;
        ssi += c.ssiDelta;

        // Next point is the closest vertex on the 4-simplex whose base vertex is the aforementioned vertex.
        double score0 = 1.0 + ssi * (-1.0 / 0.309016994374947); // Seems slightly faster than 1.0-xsi-ysi-zsi-wsi
        vertexIndex = 0b0000;
        if (xsi >= ysi && xsi >= zsi && xsi >= wsi && xsi >= score0) {
            vertexIndex = 0b0001;
        } else if (ysi > xsi && ysi >= zsi && ysi >= wsi && ysi >= score0) {
            vertexIndex = 0b0010;
        } else if (zsi > xsi && zsi > ysi && zsi >= wsi && zsi >= score0) {
            vertexIndex = 0b0100;
        } else if (wsi > xsi && wsi > ysi && wsi > zsi && wsi >= score0) {
            vertexIndex = 0b1000;
        }
    }

    return value;
}

void open_simplex_noise2f::init_static_data()
{
	static const std::array<lattice_point3d, 64> LOOKUP_3D = [] {
		std::array<lattice_point3d, 64> arr;
		for(int i=0; i<8; i++) {
			for(int j=0; j<8; j++) {
				int i1 = (i >> 0) & 1;
				int j1 = (i >> 1) & 1;
				int k1 = (i >> 2) & 1;
				int i2 = i1 ^ 1;
				int j2 = j1 ^ 1;
				int k2 = k1 ^ 1;

				switch(j) {
					// The two points within this octant, one from each of the two cubic half-lattices.
					case  0: arr[i*8 + j] = lattice_point3d(i1, j1, k1, 0, &LOOKUP_3D[i*8 + 1], &LOOKUP_3D[i*8 + 1]); break;
					case  1: arr[i*8 + j] = lattice_point3d(i1 + i2, j1 + j2, k1 + k2, 1, &LOOKUP_3D[i*8 + 2], &LOOKUP_3D[i*8 + 2]); break;
					// Each single step away on the first half-lattice.
					case  2: arr[i*8 + j] = lattice_point3d(i1 ^ 1, j1, k1, 0, &LOOKUP_3D[i*8 + 3], &LOOKUP_3D[i*8 + 6]); break;
					case  3: arr[i*8 + j] = lattice_point3d(i1, j1 ^ 1, k1, 0, &LOOKUP_3D[i*8 + 4], &LOOKUP_3D[i*8 + 5]); break;
					case  4: arr[i*8 + j] = lattice_point3d(i1, j1, k1 ^ 1, 0, &LOOKUP_3D[i*8 + 5], &LOOKUP_3D[i*8 + 5]); break;
					// Each single step away on the second half-lattice.
					case  5: arr[i*8 + j] = lattice_point3d(i1 + (i2 ^ 1), j1 + j2, k1 + k2, 1, &LOOKUP_3D[i*8 + 6], nullptr); break;
					case  6: arr[i*8 + j] = lattice_point3d(i1 + i2, j1 + (j2 ^ 1), k1 + k2, 1, &LOOKUP_3D[i*8 + 7], nullptr); break;
					case  7: arr[i*8 + j] = lattice_point3d(i1 + i2, j1 + j2, k1 + (k2 ^ 1), 1, nullptr, nullptr); break;
				}
			}
		}
		return arr;
	}();

	static const std::array<lattice_point4df, 16> LOOKUP_4D = [] {
		std::array<lattice_point4df, 16> arr;
		for (int i = 0; i < 16; i++) {
			arr[i] = lattice_point4df((i >> 0) & 1, (i >> 1) & 1, (i >> 2) & 1, (i >> 3) & 1);
		}
		return arr;
	}();

	static const std::array<grad2, PSIZE> GRADIENTS_2D = [] {
		grad2 arr2[] = {
			grad2( 0.130526192220052,  0.99144486137381),
			grad2( 0.38268343236509,   0.923879532511287),
			grad2( 0.608761429008721,  0.793353340291235),
			grad2( 0.793353340291235,  0.608761429008721),
			grad2( 0.923879532511287,  0.38268343236509),
			grad2( 0.99144486137381,   0.130526192220051),
			grad2( 0.99144486137381,  -0.130526192220051),
			grad2( 0.923879532511287, -0.38268343236509),
			grad2( 0.793353340291235, -0.60876142900872),
			grad2( 0.608761429008721, -0.793353340291235),
			grad2( 0.38268343236509,  -0.923879532511287),
			grad2( 0.130526192220052, -0.99144486137381),
			grad2(-0.130526192220052, -0.99144486137381),
			grad2(-0.38268343236509,  -0.923879532511287),
			grad2(-0.608761429008721, -0.793353340291235),
			grad2(-0.793353340291235, -0.608761429008721),
			grad2(-0.923879532511287, -0.38268343236509),
			grad2(-0.99144486137381,  -0.130526192220052),
			grad2(-0.99144486137381,   0.130526192220051),
			grad2(-0.923879532511287,  0.38268343236509),
			grad2(-0.793353340291235,  0.608761429008721),
			grad2(-0.608761429008721,  0.793353340291235),
			grad2(-0.38268343236509,   0.923879532511287),
			grad2(-0.130526192220052,  0.99144486137381)
		};
		int n = sizeof(arr2)/sizeof(grad2);
		for (int i = 0; i < n; i++) {
			arr2[i].dx /= N2;
			arr2[i].dy /= N2;
		}

		std::array<grad2, PSIZE> grad2d;
		for (size_t i = 0; i < PSIZE; i++) {
			grad2d[i] = arr2[i % n];
		}
		return grad2d;
	}();

	static const std::array<grad3, PSIZE> GRADIENTS_3D = [] {
		grad3 arr3[] = {
			grad3(-2.22474487139,      -2.22474487139,      -1.0),
			grad3(-2.22474487139,      -2.22474487139,       1.0),
			grad3(-3.0862664687972017, -1.1721513422464978,  0.0),
			grad3(-1.1721513422464978, -3.0862664687972017,  0.0),
			grad3(-2.22474487139,      -1.0,                -2.22474487139),
			grad3(-2.22474487139,       1.0,                -2.22474487139),
			grad3(-1.1721513422464978,  0.0,                -3.0862664687972017),
			grad3(-3.0862664687972017,  0.0,                -1.1721513422464978),
			grad3(-2.22474487139,      -1.0,                 2.22474487139),
			grad3(-2.22474487139,       1.0,                 2.22474487139),
			grad3(-3.0862664687972017,  0.0,                 1.1721513422464978),
			grad3(-1.1721513422464978,  0.0,                 3.0862664687972017),
			grad3(-2.22474487139,       2.22474487139,      -1.0),
			grad3(-2.22474487139,       2.22474487139,       1.0),
			grad3(-1.1721513422464978,  3.0862664687972017,  0.0),
			grad3(-3.0862664687972017,  1.1721513422464978,  0.0),
			grad3(-1.0,                -2.22474487139,      -2.22474487139),
			grad3( 1.0,                -2.22474487139,      -2.22474487139),
			grad3( 0.0,                -3.0862664687972017, -1.1721513422464978),
			grad3( 0.0,                -1.1721513422464978, -3.0862664687972017),
			grad3(-1.0,                -2.22474487139,       2.22474487139),
			grad3( 1.0,                -2.22474487139,       2.22474487139),
			grad3( 0.0,                -1.1721513422464978,  3.0862664687972017),
			grad3( 0.0,                -3.0862664687972017,  1.1721513422464978),
			grad3(-1.0,                 2.22474487139,      -2.22474487139),
			grad3( 1.0,                 2.22474487139,      -2.22474487139),
			grad3( 0.0,                 1.1721513422464978, -3.0862664687972017),
			grad3( 0.0,                 3.0862664687972017, -1.1721513422464978),
			grad3(-1.0,                 2.22474487139,       2.22474487139),
			grad3( 1.0,                 2.22474487139,       2.22474487139),
			grad3( 0.0,                 3.0862664687972017,  1.1721513422464978),
			grad3( 0.0,                 1.1721513422464978,  3.0862664687972017),
			grad3( 2.22474487139,      -2.22474487139,      -1.0),
			grad3( 2.22474487139,      -2.22474487139,       1.0),
			grad3( 1.1721513422464978, -3.0862664687972017,  0.0),
			grad3( 3.0862664687972017, -1.1721513422464978,  0.0),
			grad3( 2.22474487139,      -1.0,                -2.22474487139),
			grad3( 2.22474487139,       1.0,                -2.22474487139),
			grad3( 3.0862664687972017,  0.0,                -1.1721513422464978),
			grad3( 1.1721513422464978,  0.0,                -3.0862664687972017),
			grad3( 2.22474487139,      -1.0,                 2.22474487139),
			grad3( 2.22474487139,       1.0,                 2.22474487139),
			grad3( 1.1721513422464978,  0.0,                 3.0862664687972017),
			grad3( 3.0862664687972017,  0.0,                 1.1721513422464978),
			grad3( 2.22474487139,       2.22474487139,      -1.0),
			grad3( 2.22474487139,       2.22474487139,       1.0),
			grad3( 3.0862664687972017,  1.1721513422464978,  0.0),
			grad3( 1.1721513422464978,  3.0862664687972017,  0.0)
		};
		int n = sizeof(arr3)/sizeof(grad3);
		for (int i = 0; i < n; i++) {
			arr3[i].dx /= N3;
			arr3[i].dy /= N3;
			arr3[i].dz /= N3;
		}

		std::array<grad3, PSIZE> grad3d;
		for (size_t i = 0; i < PSIZE; i++) {
			grad3d[i] = arr3[i % n];
		}
		return grad3d;
	}();

	static const std::array<grad4, PSIZE> GRADIENTS_4D = [] {
		grad4 arr4[] = {
			grad4(-0.753341017856078,    -0.37968289875261624,  -0.37968289875261624,  -0.37968289875261624),
			grad4(-0.7821684431180708,   -0.4321472685365301,   -0.4321472685365301,    0.12128480194602098),
			grad4(-0.7821684431180708,   -0.4321472685365301,    0.12128480194602098,  -0.4321472685365301),
			grad4(-0.7821684431180708,    0.12128480194602098,  -0.4321472685365301,   -0.4321472685365301),
			grad4(-0.8586508742123365,   -0.508629699630796,     0.044802370851755174,  0.044802370851755174),
			grad4(-0.8586508742123365,    0.044802370851755174, -0.508629699630796,     0.044802370851755174),
			grad4(-0.8586508742123365,    0.044802370851755174,  0.044802370851755174, -0.508629699630796),
			grad4(-0.9982828964265062,   -0.03381941603233842,  -0.03381941603233842,  -0.03381941603233842),
			grad4(-0.37968289875261624,  -0.753341017856078,    -0.37968289875261624,  -0.37968289875261624),
			grad4(-0.4321472685365301,   -0.7821684431180708,   -0.4321472685365301,    0.12128480194602098),
			grad4(-0.4321472685365301,   -0.7821684431180708,    0.12128480194602098,  -0.4321472685365301),
			grad4( 0.12128480194602098,  -0.7821684431180708,   -0.4321472685365301,   -0.4321472685365301),
			grad4(-0.508629699630796,    -0.8586508742123365,    0.044802370851755174,  0.044802370851755174),
			grad4( 0.044802370851755174, -0.8586508742123365,   -0.508629699630796,     0.044802370851755174),
			grad4( 0.044802370851755174, -0.8586508742123365,    0.044802370851755174, -0.508629699630796),
			grad4(-0.03381941603233842,  -0.9982828964265062,   -0.03381941603233842,  -0.03381941603233842),
			grad4(-0.37968289875261624,  -0.37968289875261624,  -0.753341017856078,    -0.37968289875261624),
			grad4(-0.4321472685365301,   -0.4321472685365301,   -0.7821684431180708,    0.12128480194602098),
			grad4(-0.4321472685365301,    0.12128480194602098,  -0.7821684431180708,   -0.4321472685365301),
			grad4( 0.12128480194602098,  -0.4321472685365301,   -0.7821684431180708,   -0.4321472685365301),
			grad4(-0.508629699630796,     0.044802370851755174, -0.8586508742123365,    0.044802370851755174),
			grad4( 0.044802370851755174, -0.508629699630796,    -0.8586508742123365,    0.044802370851755174),
			grad4( 0.044802370851755174,  0.044802370851755174, -0.8586508742123365,   -0.508629699630796),
			grad4(-0.03381941603233842,  -0.03381941603233842,  -0.9982828964265062,   -0.03381941603233842),
			grad4(-0.37968289875261624,  -0.37968289875261624,  -0.37968289875261624,  -0.753341017856078),
			grad4(-0.4321472685365301,   -0.4321472685365301,    0.12128480194602098,  -0.7821684431180708),
			grad4(-0.4321472685365301,    0.12128480194602098,  -0.4321472685365301,   -0.7821684431180708),
			grad4( 0.12128480194602098,  -0.4321472685365301,   -0.4321472685365301,   -0.7821684431180708),
			grad4(-0.508629699630796,     0.044802370851755174,  0.044802370851755174, -0.8586508742123365),
			grad4( 0.044802370851755174, -0.508629699630796,     0.044802370851755174, -0.8586508742123365),
			grad4( 0.044802370851755174,  0.044802370851755174, -0.508629699630796,    -0.8586508742123365),
			grad4(-0.03381941603233842,  -0.03381941603233842,  -0.03381941603233842,  -0.9982828964265062),
			grad4(-0.6740059517812944,   -0.3239847771997537,   -0.3239847771997537,    0.5794684678643381),
			grad4(-0.7504883828755602,   -0.4004672082940195,    0.15296486218853164,   0.5029860367700724),
			grad4(-0.7504883828755602,    0.15296486218853164,  -0.4004672082940195,    0.5029860367700724),
			grad4(-0.8828161875373585,    0.08164729285680945,   0.08164729285680945,   0.4553054119602712),
			grad4(-0.4553054119602712,   -0.08164729285680945,  -0.08164729285680945,   0.8828161875373585),
			grad4(-0.5029860367700724,   -0.15296486218853164,   0.4004672082940195,    0.7504883828755602),
			grad4(-0.5029860367700724,    0.4004672082940195,   -0.15296486218853164,   0.7504883828755602),
			grad4(-0.5794684678643381,    0.3239847771997537,    0.3239847771997537,    0.6740059517812944),
			grad4(-0.3239847771997537,   -0.6740059517812944,   -0.3239847771997537,    0.5794684678643381),
			grad4(-0.4004672082940195,   -0.7504883828755602,    0.15296486218853164,   0.5029860367700724),
			grad4( 0.15296486218853164,  -0.7504883828755602,   -0.4004672082940195,    0.5029860367700724),
			grad4( 0.08164729285680945,  -0.8828161875373585,    0.08164729285680945,   0.4553054119602712),
			grad4(-0.08164729285680945,  -0.4553054119602712,   -0.08164729285680945,   0.8828161875373585),
			grad4(-0.15296486218853164,  -0.5029860367700724,    0.4004672082940195,    0.7504883828755602),
			grad4( 0.4004672082940195,   -0.5029860367700724,   -0.15296486218853164,   0.7504883828755602),
			grad4( 0.3239847771997537,   -0.5794684678643381,    0.3239847771997537,    0.6740059517812944),
			grad4(-0.3239847771997537,   -0.3239847771997537,   -0.6740059517812944,    0.5794684678643381),
			grad4(-0.4004672082940195,    0.15296486218853164,  -0.7504883828755602,    0.5029860367700724),
			grad4( 0.15296486218853164,  -0.4004672082940195,   -0.7504883828755602,    0.5029860367700724),
			grad4( 0.08164729285680945,   0.08164729285680945,  -0.8828161875373585,    0.4553054119602712),
			grad4(-0.08164729285680945,  -0.08164729285680945,  -0.4553054119602712,    0.8828161875373585),
			grad4(-0.15296486218853164,   0.4004672082940195,   -0.5029860367700724,    0.7504883828755602),
			grad4( 0.4004672082940195,   -0.15296486218853164,  -0.5029860367700724,    0.7504883828755602),
			grad4( 0.3239847771997537,    0.3239847771997537,   -0.5794684678643381,    0.6740059517812944),
			grad4(-0.6740059517812944,   -0.3239847771997537,    0.5794684678643381,   -0.3239847771997537),
			grad4(-0.7504883828755602,   -0.4004672082940195,    0.5029860367700724,    0.15296486218853164),
			grad4(-0.7504883828755602,    0.15296486218853164,   0.5029860367700724,   -0.4004672082940195),
			grad4(-0.8828161875373585,    0.08164729285680945,   0.4553054119602712,    0.08164729285680945),
			grad4(-0.4553054119602712,   -0.08164729285680945,   0.8828161875373585,   -0.08164729285680945),
			grad4(-0.5029860367700724,   -0.15296486218853164,   0.7504883828755602,    0.4004672082940195),
			grad4(-0.5029860367700724,    0.4004672082940195,    0.7504883828755602,   -0.15296486218853164),
			grad4(-0.5794684678643381,    0.3239847771997537,    0.6740059517812944,    0.3239847771997537),
			grad4(-0.3239847771997537,   -0.6740059517812944,    0.5794684678643381,   -0.3239847771997537),
			grad4(-0.4004672082940195,   -0.7504883828755602,    0.5029860367700724,    0.15296486218853164),
			grad4( 0.15296486218853164,  -0.7504883828755602,    0.5029860367700724,   -0.4004672082940195),
			grad4( 0.08164729285680945,  -0.8828161875373585,    0.4553054119602712,    0.08164729285680945),
			grad4(-0.08164729285680945,  -0.4553054119602712,    0.8828161875373585,   -0.08164729285680945),
			grad4(-0.15296486218853164,  -0.5029860367700724,    0.7504883828755602,    0.4004672082940195),
			grad4( 0.4004672082940195,   -0.5029860367700724,    0.7504883828755602,   -0.15296486218853164),
			grad4( 0.3239847771997537,   -0.5794684678643381,    0.6740059517812944,    0.3239847771997537),
			grad4(-0.3239847771997537,   -0.3239847771997537,    0.5794684678643381,   -0.6740059517812944),
			grad4(-0.4004672082940195,    0.15296486218853164,   0.5029860367700724,   -0.7504883828755602),
			grad4( 0.15296486218853164,  -0.4004672082940195,    0.5029860367700724,   -0.7504883828755602),
			grad4( 0.08164729285680945,   0.08164729285680945,   0.4553054119602712,   -0.8828161875373585),
			grad4(-0.08164729285680945,  -0.08164729285680945,   0.8828161875373585,   -0.4553054119602712),
			grad4(-0.15296486218853164,   0.4004672082940195,    0.7504883828755602,   -0.5029860367700724),
			grad4( 0.4004672082940195,   -0.15296486218853164,   0.7504883828755602,   -0.5029860367700724),
			grad4( 0.3239847771997537,    0.3239847771997537,    0.6740059517812944,   -0.5794684678643381),
			grad4(-0.6740059517812944,    0.5794684678643381,   -0.3239847771997537,   -0.3239847771997537),
			grad4(-0.7504883828755602,    0.5029860367700724,   -0.4004672082940195,    0.15296486218853164),
			grad4(-0.7504883828755602,    0.5029860367700724,    0.15296486218853164,  -0.4004672082940195),
			grad4(-0.8828161875373585,    0.4553054119602712,    0.08164729285680945,   0.08164729285680945),
			grad4(-0.4553054119602712,    0.8828161875373585,   -0.08164729285680945,  -0.08164729285680945),
			grad4(-0.5029860367700724,    0.7504883828755602,   -0.15296486218853164,   0.4004672082940195),
			grad4(-0.5029860367700724,    0.7504883828755602,    0.4004672082940195,   -0.15296486218853164),
			grad4(-0.5794684678643381,    0.6740059517812944,    0.3239847771997537,    0.3239847771997537),
			grad4(-0.3239847771997537,    0.5794684678643381,   -0.6740059517812944,   -0.3239847771997537),
			grad4(-0.4004672082940195,    0.5029860367700724,   -0.7504883828755602,    0.15296486218853164),
			grad4( 0.15296486218853164,   0.5029860367700724,   -0.7504883828755602,   -0.4004672082940195),
			grad4( 0.08164729285680945,   0.4553054119602712,   -0.8828161875373585,    0.08164729285680945),
			grad4(-0.08164729285680945,   0.8828161875373585,   -0.4553054119602712,   -0.08164729285680945),
			grad4(-0.15296486218853164,   0.7504883828755602,   -0.5029860367700724,    0.4004672082940195),
			grad4( 0.4004672082940195,    0.7504883828755602,   -0.5029860367700724,   -0.15296486218853164),
			grad4( 0.3239847771997537,    0.6740059517812944,   -0.5794684678643381,    0.3239847771997537),
			grad4(-0.3239847771997537,    0.5794684678643381,   -0.3239847771997537,   -0.6740059517812944),
			grad4(-0.4004672082940195,    0.5029860367700724,    0.15296486218853164,  -0.7504883828755602),
			grad4( 0.15296486218853164,   0.5029860367700724,   -0.4004672082940195,   -0.7504883828755602),
			grad4( 0.08164729285680945,   0.4553054119602712,    0.08164729285680945,  -0.8828161875373585),
			grad4(-0.08164729285680945,   0.8828161875373585,   -0.08164729285680945,  -0.4553054119602712),
			grad4(-0.15296486218853164,   0.7504883828755602,    0.4004672082940195,   -0.5029860367700724),
			grad4( 0.4004672082940195,    0.7504883828755602,   -0.15296486218853164,  -0.5029860367700724),
			grad4( 0.3239847771997537,    0.6740059517812944,    0.3239847771997537,   -0.5794684678643381),
			grad4( 0.5794684678643381,   -0.6740059517812944,   -0.3239847771997537,   -0.3239847771997537),
			grad4( 0.5029860367700724,   -0.7504883828755602,   -0.4004672082940195,    0.15296486218853164),
			grad4( 0.5029860367700724,   -0.7504883828755602,    0.15296486218853164,  -0.4004672082940195),
			grad4( 0.4553054119602712,   -0.8828161875373585,    0.08164729285680945,   0.08164729285680945),
			grad4( 0.8828161875373585,   -0.4553054119602712,   -0.08164729285680945,  -0.08164729285680945),
			grad4( 0.7504883828755602,   -0.5029860367700724,   -0.15296486218853164,   0.4004672082940195),
			grad4( 0.7504883828755602,   -0.5029860367700724,    0.4004672082940195,   -0.15296486218853164),
			grad4( 0.6740059517812944,   -0.5794684678643381,    0.3239847771997537,    0.3239847771997537),
			grad4( 0.5794684678643381,   -0.3239847771997537,   -0.6740059517812944,   -0.3239847771997537),
			grad4( 0.5029860367700724,   -0.4004672082940195,   -0.7504883828755602,    0.15296486218853164),
			grad4( 0.5029860367700724,    0.15296486218853164,  -0.7504883828755602,   -0.4004672082940195),
			grad4( 0.4553054119602712,    0.08164729285680945,  -0.8828161875373585,    0.08164729285680945),
			grad4( 0.8828161875373585,   -0.08164729285680945,  -0.4553054119602712,   -0.08164729285680945),
			grad4( 0.7504883828755602,   -0.15296486218853164,  -0.5029860367700724,    0.4004672082940195),
			grad4( 0.7504883828755602,    0.4004672082940195,   -0.5029860367700724,   -0.15296486218853164),
			grad4( 0.6740059517812944,    0.3239847771997537,   -0.5794684678643381,    0.3239847771997537),
			grad4( 0.5794684678643381,   -0.3239847771997537,   -0.3239847771997537,   -0.6740059517812944),
			grad4( 0.5029860367700724,   -0.4004672082940195,    0.15296486218853164,  -0.7504883828755602),
			grad4( 0.5029860367700724,    0.15296486218853164,  -0.4004672082940195,   -0.7504883828755602),
			grad4( 0.4553054119602712,    0.08164729285680945,   0.08164729285680945,  -0.8828161875373585),
			grad4( 0.8828161875373585,   -0.08164729285680945,  -0.08164729285680945,  -0.4553054119602712),
			grad4( 0.7504883828755602,   -0.15296486218853164,   0.4004672082940195,   -0.5029860367700724),
			grad4( 0.7504883828755602,    0.4004672082940195,   -0.15296486218853164,  -0.5029860367700724),
			grad4( 0.6740059517812944,    0.3239847771997537,    0.3239847771997537,   -0.5794684678643381),
			grad4( 0.03381941603233842,   0.03381941603233842,   0.03381941603233842,   0.9982828964265062),
			grad4(-0.044802370851755174, -0.044802370851755174,  0.508629699630796,     0.8586508742123365),
			grad4(-0.044802370851755174,  0.508629699630796,    -0.044802370851755174,  0.8586508742123365),
			grad4(-0.12128480194602098,   0.4321472685365301,    0.4321472685365301,    0.7821684431180708),
			grad4( 0.508629699630796,    -0.044802370851755174, -0.044802370851755174,  0.8586508742123365),
			grad4( 0.4321472685365301,   -0.12128480194602098,   0.4321472685365301,    0.7821684431180708),
			grad4( 0.4321472685365301,    0.4321472685365301,   -0.12128480194602098,   0.7821684431180708),
			grad4( 0.37968289875261624,   0.37968289875261624,   0.37968289875261624,   0.753341017856078),
			grad4( 0.03381941603233842,   0.03381941603233842,   0.9982828964265062,    0.03381941603233842),
			grad4(-0.044802370851755174,  0.044802370851755174,  0.8586508742123365,    0.508629699630796),
			grad4(-0.044802370851755174,  0.508629699630796,     0.8586508742123365,   -0.044802370851755174),
			grad4(-0.12128480194602098,   0.4321472685365301,    0.7821684431180708,    0.4321472685365301),
			grad4( 0.508629699630796,    -0.044802370851755174,  0.8586508742123365,   -0.044802370851755174),
			grad4( 0.4321472685365301,   -0.12128480194602098,   0.7821684431180708,    0.4321472685365301),
			grad4( 0.4321472685365301,    0.4321472685365301,    0.7821684431180708,   -0.12128480194602098),
			grad4( 0.37968289875261624,   0.37968289875261624,   0.753341017856078,     0.37968289875261624),
			grad4( 0.03381941603233842,   0.9982828964265062,    0.03381941603233842,   0.03381941603233842),
			grad4(-0.044802370851755174,  0.8586508742123365,   -0.044802370851755174,  0.508629699630796),
			grad4(-0.044802370851755174,  0.8586508742123365,    0.508629699630796,    -0.044802370851755174),
			grad4(-0.12128480194602098,   0.7821684431180708,    0.4321472685365301,    0.4321472685365301),
			grad4( 0.508629699630796,     0.8586508742123365,   -0.044802370851755174, -0.044802370851755174),
			grad4( 0.4321472685365301,    0.7821684431180708,   -0.12128480194602098,   0.4321472685365301),
			grad4( 0.4321472685365301,    0.7821684431180708,    0.4321472685365301,   -0.12128480194602098),
			grad4( 0.37968289875261624,   0.753341017856078,     0.37968289875261624,   0.37968289875261624),
			grad4( 0.9982828964265062,    0.03381941603233842,   0.03381941603233842,   0.03381941603233842),
			grad4( 0.8586508742123365,   -0.044802370851755174, -0.044802370851755174,  0.508629699630796),
			grad4( 0.8586508742123365,   -0.044802370851755174,  0.508629699630796,    -0.044802370851755174),
			grad4( 0.7821684431180708,   -0.12128480194602098,   0.4321472685365301,    0.4321472685365301),
			grad4( 0.8586508742123365,    0.508629699630796,    -0.044802370851755174, -0.044802370851755174),
			grad4( 0.7821684431180708,    0.4321472685365301,   -0.12128480194602098,   0.4321472685365301),
			grad4( 0.7821684431180708,    0.4321472685365301,    0.4321472685365301,   -0.12128480194602098),
			grad4( 0.753341017856078,     0.37968289875261624,   0.37968289875261624,   0.37968289875261624)
		};
		int n = sizeof(arr4)/sizeof(grad4);
		for (int i = 0; i < n; i++) {
			arr4[i].dx /= N4;
			arr4[i].dy /= N4;
			arr4[i].dz /= N4;
			arr4[i].dw /= N4;
		}
		std::array<grad4, PSIZE> grad4d;
		for (size_t i = 0; i < PSIZE; i++) {
			grad4d[i] = arr4[i % n];
		}

		return grad4d;
	}();

	this->LOOKUP_3D = &LOOKUP_3D;
	this->LOOKUP_4D = &LOOKUP_4D;
	this->GRADIENTS_2D = &GRADIENTS_2D;
	this->GRADIENTS_3D = &GRADIENTS_3D;
	this->GRADIENTS_4D = &GRADIENTS_4D;
}

