#include <algorithm>
#include <cmath>
#include "fractal_noise.hpp"

template<typename N>
fractal_noise<N>::fractal_noise(uint64_t seed)
: m_noise(seed)
{
}


template<typename N>
void fractal_noise<N>::set_seed(uint64_t seed)
{
	this->m_noise.set_seed(seed);
}

template<typename N>
double fractal_noise<N>::noise_texture_2d(double x, double y, double scale, double detail, double roughness) const
{
	return noise_2d(x * scale, y * scale, detail, roughness);
}

template<typename N>
double fractal_noise<N>::noise_2d(double x, double y, double octaves, double roughness) const
{
	double fscale = 1.0;
	double amp = 1.0;
	double maxamp = 0.0;
	double sum1 = 0.0;
	octaves = std::clamp(octaves, 0.0, 16.0);
	roughness = std::clamp(roughness, 0.0, 1.0);
	int n(octaves);
	for (int o = 0; o <= n; o++) {
		double t = m_noise.noise_2d_classic(x * fscale, y * fscale);
		sum1 += t * amp;
		maxamp += amp;
		amp *= roughness;
		fscale *= 2.0;
	}
	double rmd = octaves - std::floor(octaves);
	if (rmd != 0.0) {
		double t = m_noise.noise_2d_classic(x * fscale, y * fscale);
		double sum2 = sum1 + t * amp;
		sum1 /= maxamp;
		sum2 /= maxamp + amp;
		return (1.0 - rmd) * sum1 + rmd * sum2;
	} else {
		return sum1 / maxamp;
	}
}
