#pragma once

#include <stdint.h>

template<typename N>
class fractal_noise {

	private:
		N m_noise;


	public:
		fractal_noise(uint64_t seed);


	public:
		void set_seed(uint64_t seed);
		double noise_texture_2d(double x, double y, double scale, double detail, double roughness) const;

	private:
		double noise_2d(double x, double y, double octaves, double roughness) const;
};

#include "fractal_noise.cpp"
