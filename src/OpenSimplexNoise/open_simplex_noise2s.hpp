/**
 * K.jpg's OpenSimplex 2, smooth variant ("SuperSimplex")
 *
 * - 2D is standard simplex, modified to support larger kernels.
 *   Implemented using a lookup table.
 * - 3D is "Re-oriented 8-point BCC noise" which constructs a
 *   congruent BCC lattice in a much different way than usual.
 * - 4D uses a naïve pregenerated lookup table, and averages out
 *   to the expected performance.
 *
 * Multiple versions of each function are provided. See the
 * documentation above each, for more info.
 *
 * modified on 2020-10-18 by KTuff
 */

#pragma once

#include <stdint.h>
#include <array>
#include <vector>

#include "simplex_util.hpp"

struct lattice_point4ds {
    int xsv, ysv, zsv, wsv;
    double dx, dy, dz, dw;

    lattice_point4ds();
    lattice_point4ds(int xsv, int ysv, int zsv, int wsv);
};


class open_simplex_noise2s {

    private:
        static const int PSIZE = 2048;
        static const int PMASK = 2047;

        static const std::array<lattice_point2d, 32>* LOOKUP_2D;
        static const std::array<lattice_point3d, 112>* LOOKUP_3D;
        static const std::array<std::vector<lattice_point4ds>, 256>* LOOKUP_4D;

        static const std::array<grad2, PSIZE>* GRADIENTS_2D;
        static const std::array<grad3, PSIZE>* GRADIENTS_3D;
        static const std::array<grad4, PSIZE>* GRADIENTS_4D;

        int16_t m_perm[PSIZE];
        grad2 m_perm_grad2[PSIZE];
        grad3 m_perm_grad3[PSIZE];
        grad4 m_perm_grad4[PSIZE];


    public:
        open_simplex_noise2s(uint64_t seed);
        ~open_simplex_noise2s();


    public:
        void set_seed(uint64_t seed);

        double noise_2d_classic(double x, double y) const;
        double noise_2d_XbeforeY(double x, double y) const;

        double noise_3d_classic(double x, double y, double z) const;
        double noise_3d_XYbeforeZ(double x, double y, double z) const;
        double noise_3d_XZbeforeY(double x, double y, double z) const;

        double noise_4d_classic(double x, double y, double z, double w) const;
        double noise_4d_XYbeforeZW(double x, double y, double z, double w) const;
        double noise_4d_XZbeforeYW(double x, double y, double z, double w) const;
        double noise_4d_XYZbeforeW(double x, double y, double z, double w) const;

    private:
        double noise_2d_base(double x, double y) const;
        double noise_3d_base(double x, double y, double z) const;
        double noise_4d_base(double x, double y, double z, double w) const;

	private:
		void init_static_data();
};
