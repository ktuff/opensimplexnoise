#pragma once

#include <stdint.h>

class open_simplex_noise {

    private:
        static const int8_t gradients2D[16];
        static const int8_t gradients3D[72];
        static const int8_t gradients4D[256];

        int16_t* perm;
        int16_t* permGradIndex3D;


    public:
        open_simplex_noise(uint64_t seed);
        ~open_simplex_noise();


    public:
        void set_seed(uint64_t seed);
        double noise_2d_classic(double x, double y) const;
        double noise_3d_classic(double x, double y, double z) const;
        double noise_4d_classic(double x, double y, double z, double w) const;

    private:
        double extrapolate2(int xsb, int ysb, double dx, double dy) const;
        double extrapolate3(int xsb, int ysb, int zsb, double dx, double dy, double dz) const;
        double extrapolate4(int xsb, int ysb, int zsb, int wsb, double dx, double dy, double dz, double dw) const;
        inline int fast_floor(double x) const;
};

